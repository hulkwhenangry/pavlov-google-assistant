This is a prototype google assistant app for administering my pavlov shack server.
I thought some admins might be interested in how this works, and I see there are a few other methods out there - so maybe someone will be interested in it.

Huge disclaimer:  I am self-taught in node.js.  The most obvious sign of this is I struggled with getting netcat to work with async/await.
So I ended up creating node.js script to write to a file, then the node server reading the file.  I hate this, but fixing it is beyond my skills/capacity.
I have a job, I did this for myself, I thought saying "ok google spawn rocket launchers for everyone" would be cool.  Kicking toxic people is also high on my list.

The google assistant portion is in Alpha, where I don't need google approval to publish, but I can add a limited set of users.
I'm still working on adding features to make admin of my own server easy, I haven't thought much about how to abstract to enable one google assistant support multiple admins administering one or more servers.  I'm not sure I'll ever get there.

To initiate the Pavlov Suncoast, I say "Ok google, talk to Pavlov Suncoast".  
Sometimes google is finicky and I need to say it multiple times.

Current commands:
* Server status - Indicate the map, mode, and number of users on the server
* Switch Map - prompts for map name and map mode.
* Rotate Map - cycle to next map in rotation
* List users - List of user names on the server.
* Spawn <item> - Spawn an item.  Currently hard-coded to admin id, until I add other/all user support.

My prioritized backlog is here:
https://gitlab.com/pbossman/pavlov-google-assistant/-/boards

How does it work?
The command comes into google, it's natural language maps the command to an intent.  I also have entities defined.
For example, all the spawnable items are an entity with a list of synonyms to map to the actual pavlov item name.
All of the maps and modes are explicit entities with synonyms mapped.
This is all in the assistant folder.

After google determines intent and entities, it will fullfill the request.
An intent is what you intend to do - each of the commands above is defined as an intent, and may have one or more entities associated with it.  It is possible for some simple Q&A to be handled entirely in google (what is an ak47?  What is TTT?).  One of the interesting things with this interface is handling natural language misses.  I had to add McDonalds, MacDonalds, and McDonald, Mickey D's - all as synonyms for the McDonalds map.  If you see strange synonyms for intent/entity matching, it's because things sound similar and weren't matching. 

Simple questions/answers can be documented right in dialogflow.
To administer, we need to issue RCON commands.  Intents backed by RCON commands require a webhook, defined in fullfillment.  This points to the node server.  Google makes a rest call to the endpoint and passes a json payload, with the intent and parameters (map/mode, item to spawn, etc).  It's really just a bunch of json files, easily readable. 

The pavlov assistant backend server has a set of intent handlers that build the command, execute the command, and provide a response.
Google assistant apps are intended to be conversational, so every response has to finish with a question, eg. "anything else I can do?", or you need to click "end conversation" to close the google assistant session.  Basically, this prevents your app from leaving an unexpected (to the user) open microphone.  Applications that leave unexpected open microphone are blocked from publication (but not Alpha! :-) ).
Google also expects the backend server to provide a response quickly, I think it's 3 seconds but I'm not sure.
If you've defined "default response", assistant will return this if it doesn't get a response in time.  Otherwise, it gives a generic "server not responding" and ends the conversation.  Note - in timeout cases, it is possible the command completes or fails...

My first server, I had poor/unreliable RCON performance, and it resulted in a lot of timeouts and uncertainty whether the command executed or not.
Basically, if you want to do this, good RCON performance is important.

Google Assistant account details:
It requires a google account, creating a project in google actions console, and associating that with a dialogflow agent, then importing the intents/entities, setting fullfillment to point to the backend server.
https://console.actions.google.com/

Import/export of dialogflow artifacts is here:
https://dialogflow.cloud.google.com/#/editAgent/<google project name>/

