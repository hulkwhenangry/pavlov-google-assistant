// include actions on google library
const {
  dialogflow,
  actionssdk,
  Image,
  Table,
  Carousel,
  List
} = require("actions-on-google");

const path = require('path');
const fs = require('fs');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
let env = process.env.NODE_ENV || 'pavlovhorde';
let config = require('../config/config.js')[env];
let uniqueId = config.rcon.defaultUniqueId;

// perform RCON command
// I couldn't get netcat to work reliably via async, so this calls a node.js script which issues the
// RCON commands and writes the result to a file, which I then read in.  :-/
// I really wish I understood async/await enough to eliminate using files.
async function rconCmd(cmd, filename) {
  let respData = '';
  console.log('execute rconCmd: ' + cmd);
  const {stdout, stderr } = await exec(`node ./scripts/rconCmd.js ` + cmd + ` ` + filename);
  respData = await fs.readFileSync('./tmp/' + filename);
  console.log(respData);
  if (respData.length > 0) {
    return JSON.parse(respData);
  } else
  {
    return respData;
  }
};

let dateFormat = require('dateformat');
// instantiate dialog flow client
const app = dialogflow({debug: false});

// Handlers go here.
app.intent("Default Welcome Intent", conv => {
   conv.ask("Welcome to the Suncoast Pavlov Admin assistant, what would you like me to do?");
  });

// currently spawns to hard-coded admin id.
app.intent("spawnItem", async conv => {
  let respString = '';
  let respData = '';
  let spawnItem = conv.body.queryResult.parameters.item;
  let num = 1;
  let cmdText = `"GiveItem ` + uniqueId + ' ' + spawnItem + `"`;
  let filename = 'GiveItem' + num + '.json';
  console.log("Start Spawn Item intent");
  console.log('spawn item: ' + spawnItem);
  respData = await rconCmd (cmdText, filename);
  console.log('Spawn item command executed');
  console.log(respData);
  if (respData.length == 0) {
    respString = "Sorry, no response from the server, spawn may have failed.  ";
  } else if (respData.GiveItem) {
      respString = spawnItem + ' spawned successfully, ';
  } else {
      respString = ' sorry, spawn ' + spawnItem + ' failed, ';
  }
  respString = respString + ' what else can I help you with?';
  console.log(respString);
  conv.ask(respString);
});

// Indicates what users are on the sysem and logs it.
app.intent("listUsers", async conv => {
  let respString = '';
  let tmpUserName = '';
  let userLog = '';
  let cmdText = `"RefreshList"`;
  let filename = `refreshList.json`;
  console.log("Tripped getUsers intent");
  const userInfo = await rconCmd (cmdText, filename);
  console.log('build response');
  console.log(userInfo);
  if (userInfo.length == 0) {
    respString = "Sorry, Suncoast server didn't respond in time, ";
    console.log(respString);
  } else 
  {
    if (userInfo.PlayerList.length == 0) {
      respString = 'There are no active players at this time, ';
    }
    else {
      console.log('code to handle users') ;
      if(userInfo.PlayerList.length == 1 ) {
        respString = 'There is 1 user playing. It is ' ;
        respString = respString + userInfo.PlayerList[0].Username.slice(4,userInfo.PlayerList[0].Username.length);
        respString = respString + ', ';
        userLog = userInfo.PlayerList[0].Username + ' ' + userInfo.PlayerList[0].UniqueId + '\n' ;
      } else 
      {
        respString = 'There are ' + userInfo.PlayerList.length + ' users playing. They are ';
        for (let index = 0; index < userInfo.PlayerList.length; ++index) {
          tmpUserName = userInfo.PlayerList[index].Username.slice(4,userInfo.PlayerList[index].Username.length);
          respString = respString + tmpUserName + ' , ';
          userLog = userInfo.PlayerList[index].Username + ' ' + userInfo.PlayerList[index].UniqueId + '\n';
        }
      }
    }
  }
  fs.writeFileSync('./tmp/userLog' + Date.now() + '.txt', userLog, function (err,data) {
    if (err) {
      return console.log(err);
    }
    console.log(data);
  });
respString = respString + 'is there anything else I can help you with?';
conv.ask(respString);
});

// return server information
app.intent("serverInfo", async conv => {
  let cmdText = `"ServerInfo"`;
  let filename = `serverInfo.json`;
  let respString = '';
  let gameMode = '';
  console.log("Tripped serverInfo intent");
  const servInf = await rconCmd (cmdText, filename);
  console.log('build response');
  console.log(servInf);
  if (servInf.length == 0) {
    respString = "Sorry, Suncoast server didn't respond in time, is there anything else I can help you with?";
    console.log(respString);
  } else 
  {
    let pos = servInf.ServerInfo.PlayerCount.indexOf("/");
    let playerCount = servInf.ServerInfo.PlayerCount.slice(0,pos);
    let maxPlayers = servInf.ServerInfo.PlayerCount.slice(pos+1,servInf.ServerInfo.PlayerCount.length);
    pos = servInf.ServerInfo.MapLabel.lastIndexOf("_");
    let map = servInf.ServerInfo.MapLabel.slice(pos+1,servInf.ServerInfo.MapLabel.length);
    switch(servInf.ServerInfo.GameMode) {
      case 'DM': 
        gameMode = 'Death Match'
        break;
      case 'TTT':
        gameMode = 'TTT Trouble in Terrorist Town'
        break;
      case 'TDM':
        gameMode = 'Team Death Match'
        break;
      case 'SND':
        gameMode = 'Search and Destroy'
        break;
      case 'ZWV':
        gameMode = 'Zombies'
        break;
      case 'GUN':
        gameMode = 'Gun Game'
        break;
      default:
        gameMode = 'Custom'
      }
      respString = 'Suncoast server is set to ' + map + ' map in ' + gameMode + ' game mode. ';
      if (playerCount == 0) {
        respString = respString + 'The server is currently empty.';
      }
      else {
        respString = respString + 'There are ' + playerCount + ' out of ' + maxPlayers + ' players.';
      }
  }
  conv.ask(respString);
});

app.intent("switchMap", async conv => {
  let respString = '';
  let targetMap = conv.body.queryResult.parameters.map;
  let targetMode = conv.body.queryResult.parameters.MapMode;
  let cmdText = `"SwitchMap ` + targetMap + ' ' + targetMode + `"`;
  let filename = 'switchMap.json';
  const switchMapResp = await rconCmd (cmdText, filename);
  if (switchMapResp.length == 0) {
    respString = 'Server not responding, not sure if map switch is successful.';
  } else {
    if (switchMapResp.SwitchMap) {
      respString = "Map switching to " + targetMap + ' ' + targetMode;
    }
    else {
      respString = 'Map switch failed. ';
    }
  }

  respString = respString + '  Anything else I can do for you?';
  console.log(respString);
  conv.ask(respString);
});

app.intent("rotateMap", async conv => {
  let respString = '';
  let cmdText = `"RotateMap"`;
  let filename = `rotateMap.json`;
  console.log("Start rotate map intent");
  const rotateMapResp = await rconCmd (cmdText, filename);
  console.log('rotateMapResp: ');
  console.log(rotateMapResp);
  if (rotateMapResp.length == 0) {
    respString = 'Server not responding, not sure if map rotation is successful.';
  } else {
    if (rotateMapResp.RotateMap) {
      respString = "Map rotating";
    }
    else {
      respString = 'Map rotate failed. ';
    }
  }

  respString = respString + '  Anything else I can do for you?';
  console.log(respString);
  conv.ask(respString);
});

// export app
module.exports = app;