'use strict'
const express = require('express');
const path = require('path');
const app = express();
const port = 8080;
const fs = require('fs');
const https = require('https');
const bodyParser = require('body-parser');
const dialogFlowApp = require("./aog");

const privateKey = fs.readFileSync('/home/suncoast/projects/.certs/privkey.pem', 'utf8');
const certificate = fs.readFileSync('/home/suncoast/projects/.certs/fullchain.pem', 'utf8');
const credentials = {
	key: privateKey,
	cert: certificate
};

// These two following lines ensures that every incomming request
// is parsed to json automatically
app.use(bodyParser.urlencoded({ extended: 'true' }));
app.use(bodyParser.json());
app.use('/', express.static('public'));

// Google assistant sends post request to /pavlov, which gets handled here.
// this code will execute the index.js code within webhook directory.
app.post('/aog', dialogFlowApp);

const httpsServer = https.createServer(credentials, app);
httpsServer.listen(port, () => {
	console.log('HTTPS Server running on port ' + port);
});