var config = {
    server1: {
        //Rcon connection settings
        rcon: {
            hostip:   "&server1-ip",
            port:     &server1-port,
            password: "&server1-password-md5hash",
            timeout:  500,
            defaultUniqueId: "your unique id here"
        }
    },
    server2: {
        //Rcon connection settings
        rcon: {
            hostip:   "&server2-ip",
            port:     &server2-port,
            password: "server2-password-md5hash",
            timeout:  500,
            defaultUniqueId: "your unique id here"
        }
    }
};
module.exports = config;